var basePaths = {
    src: 'source/',
    dest: 'assets/',
};

var paths = {
    scripts: {
        src: basePaths.src + 'js/',
        dest: basePaths.dest + 'js/'
    },
    sass: {
        src: basePaths.src + 'scss/',
        dest: basePaths.dest + 'css/'
    }
};

var gulp = require('gulp');
var uglify = require('gulp-uglify');
var concat = require('gulp-concat');
var uglifycss = require('gulp-uglifycss');
var prefix = require('gulp-autoprefixer');
var sass = require('gulp-ruby-sass');
var merge = require('gulp-merge');
var combineMq = require('gulp-combine-mq');
var livereload = require('gulp-livereload');

gulp.task('scripts', function(){
	return gulp.src([
		paths.scripts.src + 'jmodernizr.js',
		paths.scripts.src + 'jquery.js',
		paths.scripts.src + 'custom.js'
	])
	.pipe(concat('scripts.js'))
	.pipe(uglify())
	.pipe(gulp.dest(paths.scripts.dest))
    .pipe(livereload());
});

gulp.task('sass', function() {
    return sass(
        paths.sass.src + 'style.scss'
    )
    .pipe(prefix('last 1 version'))
    .pipe(combineMq())
    .pipe(uglifycss())
    .pipe(gulp.dest(paths.sass.dest))
    .pipe(livereload());
});

gulp.task('watch', function(){
    livereload.listen();
	gulp.watch(paths.scripts.src + '*.js', ['scripts']);
	gulp.watch(paths.sass.src + '*.scss', ['sass']);
});

gulp.task('default', ['scripts', 'sass', 'watch'])