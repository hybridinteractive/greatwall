<?php

// Error reporting:

error_reporting(E_ALL^E_NOTICE);

include 'connect.php';
include 'comment.class.php';

// Default ordering.
if((isset($_GET['order_by']) and ($_GET['order_by'] == 'ASC')))
{
    $result = mysql_query('SELECT * FROM comments ORDER BY commentDate ASC');
}
else
{
    $result = mysql_query('SELECT * FROM comments ORDER BY commentDate DESC');
}

// Select all the comments and populate the $comments array with objects

$comments = array();

while($row = mysql_fetch_assoc($result))
{
    $comments[] = new Comment($row);
}

?>
<!doctype html>
<html class="no-js" lang="">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>Great Wall of Comments</title>
<meta name="description" content="">
<meta name="keywords" content="">
<link rel="stylesheet" href="/assets/css/style.css" />
</head>
<body>
<div id="container">
<h1>Comments</h1>
<p><strong>Sort:</strong> <a href="?order_by=DESC">Newest</a> | <a href="?order_by=ASC">Oldest</a></p>
<div id="addCommentContainer">
    <?php

    // Output the comments one by one:

    foreach($comments as $comment)
    {
        echo $comment->markup();
    }

    ?>
</div><!-- /#addCommentContainer -->
<form id="addCommentForm" method="post" action="">
    <h2>Leave a Comment</h2>
    <h3><span class="required">*</span> Denotes a required field</h3>
    <label for="name">Your Name <span class="required">*</span></label>
    <input type="text" name="name" id="name" />

    <label for="email">Your Email</label>
    <input type="text" name="email" id="email" />

    <label for="url">Website</label>
    <input type="text" name="url" id="url" />

    <label for="body">Comment <span class="required">*</span></label>
    <textarea name="body" id="body" cols="20" rows="5"></textarea>

    <input type="submit" id="submit" value="Submit" />
</form>
</div><!-- /#container -->
<script src="/assets/js/scripts.js"></script>
</body>
</html>