<?php

class Comment
{
	private $data = array();
	
	public function __construct($row)
	{
		// Construct the Data

		$this->data = $row;
	}
	
	public function markup()
	{
		// This method outputs the XHTML markup of the comment		
		// Setting up an alias, so we don't have to write $this->data every time:

		$d = &$this->data;
		
		$gravatar = '';
		$email = '';
		$name = '';
		$link = '';

		if(($d['email']) and ($d['url']))
		{
			$gravatar = '<div class="avatar"><img src="http://www.gravatar.com/avatar/'.md5($d['email']).'?size=50&amp;default='.urlencode($url).'" /></div>';
			$email = '<div class="name"><a href="mailto:'.$d['email'].'">'.$d['name'].'</a></div>';
			$link = '<a href="'.$d['url'].'">'.'Website'.'</a>';
		}
		elseif ((empty($d['email'])) and ($d['url']))
		{
			$link = '<div class="name"><a href="'.$d['url'].'" target="_blank">'.$d['name'].'</a></div>';
		}
		elseif ((empty($d['url'])) and ($d['email']))
		{
			$email = '<div class="name"><a href="mailto:'.$d['email'].'">'.$d['name'].'</a></div>';
		}
		else
		{
			$name = '<div class="name">'.$d['name'].'</div>';
		}
		
		// Converting the time to a UNIX timestamp:

		$d['commentDate'] = strtotime($d['commentDate']);
		
		// Needed for the default gravatar image:

		$url = 'http://'.dirname($_SERVER['SERVER_NAME'].$_SERVER["REQUEST_URI"]).'/assets/img/default_avatar.png';
		
		return '
			<div class="comment">
				'.$gravatar.'
				'.$email.$link.$name.'
				<div class="date" title="Added at '.date('H:i \o\n d M Y',$d['commentDate']).'">'.date('d M Y H:i A',$d['commentDate']).'</div>
				<p>'.$d['body'].'</p>
			</div>
		';
	}
	
	public static function validate(&$arr)
	{
		// This method is used to validate the data sent via AJAX.
		// It return true/false depending on whether the data is valid, and populates
		// the $arr array passed as a paremter (notice the ampersand above) with
		// either the valid input data, or the error messages.
		
		$errors = array();
		$data	= array();
		
		// Using the filter_input function
		
		if(!($data['url'] = filter_input(INPUT_POST,'url',FILTER_VALIDATE_URL)))
		{
			// If the URL field was not populated with a valid URL,
			// act as if no URL was entered at all:
			
			$url = '';
		}
		
		// Using the filter with a custom callback function:
		
		if(!($data['body'] = filter_input(INPUT_POST,'body',FILTER_CALLBACK,array('options'=>'Comment::validate_text'))))
		{
			$errors['body'] = '<br /><span class="required">Please enter a comment body.</span>';
		}
		
		if(!($data['name'] = filter_input(INPUT_POST,'name',FILTER_CALLBACK,array('options'=>'Comment::validate_text'))))
		{
			$errors['name'] = '<br /><span class="required">Please enter your name.</span>';
		}
		
		if(!empty($errors))
		{
			
			// If there are errors, copy the $errors array to $arr:
			
			$arr = $errors;
			return false;
		}
		
		// If the data is valid, sanitize all the data and copy it to $arr:
		
		foreach($data as $k=>$v)
		{
			$arr[$k] = mysql_real_escape_string($v);
		}
		
		// Ensure that the email is lower case:
		
		$arr['email'] = strtolower(trim($arr['email']));
		
		return true;
		
	}

	private static function validate_text($str)
	{
		// This method is used internally as a FILTER_CALLBACK
		
		if(mb_strlen($str,'utf8')<1)
		{
			return false;
		}
		
		// Encode all html special characters (<, >, ", & .. etc) and convert
		// the new line characters to <br /> tags:
		
		$str = nl2br(htmlspecialchars($str));
		
		// Remove the new line characters that are left

		$str = str_replace(array(chr(10),chr(13)),'',$str);
		
		return $str;
	}

}

?>